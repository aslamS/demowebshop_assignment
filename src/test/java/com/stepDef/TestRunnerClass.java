package com.stepDef;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/resources/Features/Demo.feature", glue = "com.stepDef",
plugin = {"html:target/cucumber-reports"},monochrome = true)
public class TestRunnerClass {

}
